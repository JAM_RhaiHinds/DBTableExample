package com.kenfogel.tests;

import com.kenfogel.entities.Gamer;
import com.kenfogel.persistence.GamingDAO;
import com.kenfogel.persistence.GamingDAOImpl;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unit test for Gamer/Games database
 *
 * @author Ken Fogel
 * @version 2.1
 */
public class TestDataBase {

    // This is my local MySQL server
    private final String url = "jdbc:mysql://localhost:3306/MULTITABLEDEMO?autoReconnect=true&useSSL=false";
    private final String user = "TheUser";
    private final String password = "pancake";

    private final Logger log = LoggerFactory.getLogger(
            this.getClass().getName());

    /**
     * This will test if the expected number of records are in the database
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testFindAll() throws SQLException {
        GamingDAO gamingDAO = new GamingDAOImpl();
        List<Gamer> gamers = gamingDAO.findAll();
        // Nothing to do with the test
        //displayAll(gamers);

        assertEquals("# of gamers", 5, gamers.size());
    }

    /**
     * This will insert a record, retrieve the just inserted record and compare
     * the inserted and retrieved objects
     *
     * @throws SQLException
     */
    @Test(timeout = 1000)
    public void testCreate() throws SQLException {
        GamingDAO gamingDAO = new GamingDAOImpl();

        Gamer g1 = new Gamer();
        g1.setFirstName("Bob");
        g1.setLastName("Smith");
        g1.setGamerHandle("Moose Man");
        g1.setMembershipDate(Timestamp.valueOf("2017-09-06 09:00:00"));
        int records = gamingDAO.create(g1);
        Gamer g2 = gamingDAO.findID(g1.getGamerID());
        assertEquals("A record was not created", g1, g2);
    }

    /**
     * This will insert a record with a field that exceeds the allowed length.
     * It should throw an exception otherwise it fails.
     *
     * @throws SQLException
     */
    @Test(timeout = 1000, expected = SQLException.class)
    public void testCreateFailureStringLength() throws SQLException {
        GamingDAO gamingDAO = new GamingDAOImpl();

        Gamer g1 = new Gamer();
        g1.setFirstName("ThisIsAVeryLongNameThatExceedsThe30CharacterLimitForName");
        g1.setLastName("Smith");
        g1.setGamerHandle("Moose Man");
        g1.setMembershipDate(Timestamp.valueOf("2017-09-06 09:00:00"));
        gamingDAO.create(g1);
        // If an exception was not thrown then the test failed
        fail("The string that exceeded the length of 30 did not throw the expected exception");
    }

    /**
     * A utility method for displaying all the records
     *
     * @param gamers
     */
    private void displayAll(List<Gamer> gamers) {
        gamers.stream().map((player) -> {
            System.out.println("" + player);
            return player;
        }).forEach((player) -> {
            player.getListOfGames().stream().forEach((games) -> {
                System.out.println("" + games);
            });
        });
    }

    /**
     * This routine recreates the database before every test. This makes sure
     * that a destructive test will not interfere with any other test. Does not
     * support stored procedures.
     *
     * This routine is courtesy of Bartosz Majsak, the lead Arquillian developer
     * at JBoss
     */
    @Before
    public void seedDatabase() {
        log.info("Seeding Database");
        final String seedDataScript = loadAsString("CreateGamerTables.sql");
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            for (String statement : splitStatements(new StringReader(
                    seedDataScript), ";")) {
                connection.prepareStatement(statement).execute();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed seeding database", e);
        }
    }

    /**
     * The following methods support the seedDatabase method
     */
    private String loadAsString(final String path) {
        try (InputStream inputStream = Thread.currentThread()
                .getContextClassLoader().getResourceAsStream(path);
                Scanner scanner = new Scanner(inputStream);) {
            return scanner.useDelimiter("\\A").next();
        } catch (IOException e) {
            throw new RuntimeException("Unable to close input stream.", e);
        }
    }

    private List<String> splitStatements(Reader reader,
            String statementDelimiter) {
        final BufferedReader bufferedReader = new BufferedReader(reader);
        final StringBuilder sqlStatement = new StringBuilder();
        final List<String> statements = new LinkedList<>();
        try {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                line = line.trim();
                if (line.isEmpty() || isComment(line)) {
                    continue;
                }
                sqlStatement.append(line);
                if (line.endsWith(statementDelimiter)) {
                    statements.add(sqlStatement.toString());
                    sqlStatement.setLength(0);
                }
            }
            return statements;
        } catch (IOException e) {
            throw new RuntimeException("Failed parsing sql", e);
        }
    }

    private boolean isComment(final String line) {
        return line.startsWith("--") || line.startsWith("//")
                || line.startsWith("/*");
    }
}
