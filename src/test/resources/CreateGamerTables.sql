USE MULTITABLEDEMO;

DROP TABLE IF EXISTS GAMES;
DROP TABLE IF EXISTS GAMER;

CREATE TABLE GAMER (
  GAMERID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  LASTNAME varchar(30) NOT NULL DEFAULT '',
  FIRSTNAME varchar(30) NOT NULL DEFAULT '',
  GAMERHANDLE varchar(60) NOT NULL DEFAULT '',
  MEMBERSHIPDATE datetime DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE GAMES (
  ID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  GAMERID int,
  DATEOFPURCHASE datetime DEFAULT CURRENT_TIMESTAMP,
  GAMETITLE varchar(120) NOT NULL DEFAULT '',
  PLATFORM varchar(40),
  KEY GAMERID (GAMERID),
  CONSTRAINT gamer_1 FOREIGN KEY (GAMERID) REFERENCES GAMER (GAMERID)
);


INSERT INTO GAMER (LASTNAME, FIRSTNAME, GAMERHANDLE, MEMBERSHIPDATE) values
("Wayne","Bruce","Batman","2014-01-23 9:00:00"),
("Allen","Barry","Flash","2014-02-18 9:00:00"),
("Kent","Clark","Superman","2014-05-02 9:00:00"),
("Stark","Tony","Iron Man","2014-07-14 9:00:00"),
("Banner","Bruce","Hulk","2014-11-09 9:00:00");

INSERT INTO GAMES(GAMERID,DATEOFPURCHASE,GAMETITLE,PLATFORM) values
(1, "2014-01-23 9:00:00","Halo","XBOX 360"),
(1, "2014-01-24 9:00:00","Tetris","XBOX 360"),
(1, "2014-01-25 9:00:00","Call of Duty","PS3"),
(2, "2014-02-18 9:00:00","Mafia","PS4"),
(2, "2014-02-19 9:00:00","Mario","Nintendo"),
(2, "2014-02-20 9:00:00","Bioshock","PS3"),
(2, "2014-02-21 9:00:00","Battlefield","XBOX ONE"),
(3, "2014-05-02 9:00:00","Grand Theft Auto","PS4"),
(3, "2014-05-03 9:00:00","Legend of Zelda","Nintendo"),
(3, "2014-05-04 9:00:00","Portal","XBOX 360"),
(3, "2014-05-05 9:00:00","Star Wars Battlefront","PS4"),
(3, "2014-05-06 9:00:00","Journey","PS3"),
(3, "2014-05-07 9:00:00","Rise of the Tomb Raider","XBOX ONE"),
(3, "2014-05-07 9:00:00","FIFA","PS3"),
(4, "2014-07-14 9:00:00","The Elder Scrolls V","XBOX ONE"),
(4, "2014-07-15 9:00:00","NBA 2K17","PS4"),
(5, "2014-11-09 9:00:00","Gods of War","PS3"),
(5, "2014-11-10 9:00:00","Fallout","PS4"),
(5, "2014-11-11 9:00:00","Overwatch","PS4"),
(5, "2014-11-12 9:00:00","Gears of War","XBOX ONE");
