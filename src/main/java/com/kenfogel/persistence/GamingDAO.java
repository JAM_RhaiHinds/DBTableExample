package com.kenfogel.persistence;

import com.kenfogel.entities.Gamer;
import com.kenfogel.entities.Games;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for CRUD methods
 *
 * @author Ken Fogel
 * @version 2.0
 */
public interface GamingDAO {

    // Create
    public int create(Gamer gamer) throws SQLException;

    public int create(Games games) throws SQLException;

    // Read
    public List<Gamer> findAll() throws SQLException;

    public Gamer findID(int id) throws SQLException;
    // Update

    public int update(Gamer gamer) throws SQLException;

    public int update(Games games) throws SQLException;

    // Delete
    public int deleteGamer(int ID) throws SQLException;

    public int deleteGames(int ID) throws SQLException;
}
