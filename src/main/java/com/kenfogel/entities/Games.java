package com.kenfogel.entities;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * This table models the games a gamer owns
 *
 * @author Ken Fogel
 * @version 2.0
 */
public class Games {

    private int Id;
    private int gamerId;
    private Timestamp dateOfPurchase;
    private String gameTitle;
    private String platform;

    /**
     * Default Constructor
     */
    public Games() {
        this(-1, -1, Timestamp.valueOf(LocalDateTime.now()), "", "");
    }

    /**
     * Non-default or initializing constructor
     * 
     * @param Id
     * @param gamerId
     * @param dateOfPurchase
     * @param gameTitle
     * @param platform 
     */
    public Games(int Id, int gamerId, Timestamp dateOfPurchase, String gameTitle, String platform) {
        super();
        this.Id = Id;
        this.gamerId = gamerId;
        this.dateOfPurchase = dateOfPurchase;
        this.gameTitle = gameTitle;
        this.platform = platform;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getGamerId() {
        return gamerId;
    }

    public void setGamerId(int gamerId) {
        this.gamerId = gamerId;
    }

    public Timestamp getDateOfPurchase() {
        return dateOfPurchase;
    }

    public void setDateOfPurchase(Timestamp dateOfPurchase) {
        this.dateOfPurchase = dateOfPurchase;
    }

    public String getGameTitle() {
        return gameTitle;
    }

    public void setGameTitle(String gameTitle) {
        this.gameTitle = gameTitle;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.Id;
        hash = 97 * hash + this.gamerId;
        hash = 97 * hash + Objects.hashCode(this.dateOfPurchase);
        hash = 97 * hash + Objects.hashCode(this.gameTitle);
        hash = 97 * hash + Objects.hashCode(this.platform);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Games other = (Games) obj;
        if (this.Id != other.Id) {
            return false;
        }
        if (this.gamerId != other.gamerId) {
            return false;
        }
        if (!Objects.equals(this.gameTitle, other.gameTitle)) {
            return false;
        }
        if (!Objects.equals(this.platform, other.platform)) {
            return false;
        }
        if (!Objects.equals(this.dateOfPurchase, other.dateOfPurchase)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "     Games{" + "Id=" + Id + ", gamerId=" + gamerId + ", dateOfPurchase=" + dateOfPurchase + ", gameTitle=" + gameTitle + ", platform=" + platform + '}';
    }

}
