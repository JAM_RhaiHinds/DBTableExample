package com.kenfogel.entities;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * This class models a game player and has a link to all the games they own
 *
 * @author Ken Fogel
 * @version 2.0
 */
public class Gamer {

    private int gamerID;
    private String lastName;
    private String firstName;
    private String gamerHandle;
    private Timestamp membershipDate;
    // Here is the join
    private List<Games> listOfGames;

    public Gamer() {

        this(-1, "", "", "", Timestamp.valueOf(LocalDateTime.now()));
    }

    public Gamer(int gamerID, String lastName, String firstName, String gamerHandle, Timestamp membershipDate) {
        super();
        this.gamerID = gamerID;
        this.lastName = lastName;
        this.firstName = firstName;
        this.gamerHandle = gamerHandle;
        this.membershipDate = membershipDate;
        this.listOfGames = new ArrayList<>();
    }

    public List<Games> getListOfGames() {
        return listOfGames;
    }
  
    public int getGamerID() {
        return gamerID;
    }

    public void setGamerID(int gamerID) {
        this.gamerID = gamerID;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGamerHandle() {
        return gamerHandle;
    }

    public void setGamerHandle(String gamerHandle) {
        this.gamerHandle = gamerHandle;
    }

    public Timestamp getMembershipDate() {
        return membershipDate;
    }

    public void setMembershipDate(Timestamp membershipDate) {
        this.membershipDate = membershipDate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.gamerID;
        hash = 37 * hash + Objects.hashCode(this.lastName);
        hash = 37 * hash + Objects.hashCode(this.firstName);
        hash = 37 * hash + Objects.hashCode(this.gamerHandle);
        hash = 37 * hash + Objects.hashCode(this.membershipDate);
        hash = 37 * hash + Objects.hashCode(this.listOfGames);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gamer other = (Gamer) obj;
        if (this.gamerID != other.gamerID) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.gamerHandle, other.gamerHandle)) {
            return false;
        }
        if (!Objects.equals(this.membershipDate, other.membershipDate)) {
            return false;
        }
        if (!Objects.equals(this.listOfGames, other.listOfGames)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Gamer{" + "gamerID=" + gamerID + ", lastName=" + lastName + ", firstName=" + firstName + ", gamerHandle=" + gamerHandle + ", membershipDate=" + membershipDate + '}';
    }
}
